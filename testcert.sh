#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "usage: $0 host [servername]" 2>&1
    exit 1
fi

if [ "$#" -gt 1 ]; then
    servername=$2
else
    servername=$1
fi

# Use MacOS LibreSSL for now, ignore homebrew ssl
#if [[ -x /opt/homebrew/opt/openssl/bin/openssl ]]; then
#  PATH=/opt/homebrew/opt/openssl/bin:$PATH
#fi

cafile=$(mktemp)
pemfile=$(mktemp)
trap "{ rm -f $cafile $pemfile; }" EXIT

port=443
echo openssl s_client -showcerts -connect $1:$port -servername $servername
res=$((echo | timeout 5 openssl s_client -showcerts -connect $1:$port -servername $servername > $pemfile) 2>&1)
if [ $? -ne 0 ]; then
  echo "FAIL: $res" 2>&1
  exit 1
fi

dir=$(dirname $0)
cn=$(${dir}/pem2json.py $pemfile | jq -r '.[0].subject.commonName')
san=$(${dir}/pem2json.py $pemfile | jq -r '.[0].subjectAltName.DNS | join(" ")')
na=$(${dir}/pem2json.py $pemfile | jq -r '.[0].notAfter')
if [[ ! "$cn $san" =~ "$servername" ]]; then
  echo "$servername is not '$cn' and not in SANs '$san'" 2>&1
fi

# [SRE-1033]
root_issuer_cn=$(${dir}/pem2json.py $pemfile | jq -r '.[-1].issuer.commonName')
if [[ $root_issuer_cn = "DST Root CA X3" ]]; then
  # hack to strip bad expired cert from LE chain
  ${dir}/pem2json.py --strip-last-cert $pemfile > $cafile
else
  cp $pemfile $cafile
fi

res=$(openssl verify -untrusted $cafile $pemfile 2>&1)
if [ $? -ne 0 ]; then
  echo "$res" 2>&1
  exit 1
fi

# stupid MacOS openssl does not return non-zero on expiration
if echo $res | grep "expired"; then
  echo "EXPIRED: $res" 2>&1
  exit 1
fi

echo "$1 ($servername) OK! ($na)"
exit 0
