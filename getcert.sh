#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "usage: $0 host [servername]" 2>&1
    exit 1
fi

if [ "$#" -gt 1 ]; then
    servername=$2
else
    servername=$1
fi

# MacOS LibreSSL always reports X3 expiration; lets not use it if we can avoid it
if [[ -x /opt/homebrew/opt/openssl/bin/openssl ]]; then
  PATH=/opt/homebrew/opt/openssl/bin:$PATH
fi

echo | openssl s_client -showcerts -connect $1:443 -servername $servername > server.pem
